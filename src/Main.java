import java.io.BufferedReader;
import java.io.FileReader;

import org.omg.CORBA.ORB;

public class Main {
	private static Echo ECHO;
	private static String SERVER_IOR_FNAME = "/tmp/ior";

	public static void main(String[] args) {
		connect(args);

		System.out.println("Returned:" + ECHO.echoString("Davide"));

	}

	private static void connect(String[] args) {
		try {
			// create and initialize the ORB
			ORB orb = ORB.init(args, null);

			String stringIOR;
			try (BufferedReader fileReader = new BufferedReader(new FileReader(SERVER_IOR_FNAME))) {
				stringIOR = fileReader.readLine();
			}

			// get the root naming context
			org.omg.CORBA.Object objRef = orb.string_to_object(stringIOR);
			ECHO = EchoHelper.narrow(objRef);
		} catch (Exception e) {
			System.out.println("ERROR : " + e);
			e.printStackTrace(System.out);
		}
	}
}
